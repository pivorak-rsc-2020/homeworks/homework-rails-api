# Homework #7: Rails JSON Api

Let's imagine yourself as a developer at the very beggining of the startup. We will need to implement couple resources in order to build an Interactive Quest.
![Entities](https://drive.google.com/open?id=19SXwoaZSfRV6LHuCv0V1_d5OmTIOxF2Z)

So, what needs to be done?

You have all specs set up for your MVP.

## Task description:

1. Implement CRUD operations for Quest entity.(Use materials from presentation)

   Mandatory fields: `name` and `description`.
   **Tip 0**: Try to use tdd approach

   ```ruby
     bundle exec rspec --tag tdd spec/requests
   ```

   Above command will trigger only specs with **tdd** tag.

   ```ruby
     it 'creates quest record', tdd: true do
       expect do
         post '/quests', params: quest_params
       end.to change { Quest.count }.by(1)
     end
   ```

   After each implemented spec you will need to add `, tdd: true` to next spec

   **Tip 1** UD from CRUD operations are explained in presentation as well :)

2. Integrate [Knock](https://github.com/nsarno/knock) gem

3. Before submiting merge request, make sure that the following commands give successful feedback:

   ```ruby
     bundle exec rspec
     bundle exec rubocop
   ```

------
### Extra point(+2)

1. Request spec for Update operation for CurrentLocation entity.

2. Update operation should be secured with knock(Not authenticated user should not have access to it).

3. If there is no current location for authenticated user at the time, this operation will create CurrentLocation.

4. When there is a record in the database for authenticated user with current location, update `lat` and `lon` attributes.

### Task acceptance criteria:
* `0` - Implemented functionality that pass specs and linter check.
* `1` - There are no unresolved comments in your merge request.
* `2` - Implement current location feature with specs.